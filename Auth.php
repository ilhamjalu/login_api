<?php
defined('BASEPATH') OR exit('No direct script access allowed');
  
class Auth extends CI_Controller 
{
    function __construct()
    {
        parent::__construct();
    }
  
    public function index()
    {
        echo anchor('Auth/login_with_google', 'Login dengan Akun Google');
    }
  
    public function masuk()
    {
        $haha = $this->session->userdata('login');
        $this->load->view("logout_view.php");
    }
  
    public function login_with_google()
    {
        if (!empty($this->session->userdata('login'))) {
            redirect('Auth/masuk');
        }
        
        $client = $this->get_google_client();
        $auth_url = $client->createAuthUrl();
        redirect($auth_url);
    }
 
    public function logout()
    {
        $this->session->set_userdata('login', null);
 
        redirect('auth');
    }
  
    	
public function google()
{
    if (!empty($this->session->userdata('login')) OR empty($_GET['code'])) {
        redirect('Auth/masuk');
    }
 
    $client = $this->get_google_client();
    $client->authenticate($_GET['code']);
 
    $plus = new Google_Service_Plus($client);
    $profile = $plus->people->get("me");
     
 
    $this->session->set_userdata('login', $user);
 
    redirect('Auth/masuk');
}
  
    private function get_google_client()
    {
        $client = new Google_Client();
        $client->setAuthConfigFile(APPPATH . 'client_secret.json'); //rename file ini supaya lebih aman nanti
        $client->setRedirectUri("http://localhost/Login_API_Google/Auth.php");
        $client->setScopes(array(
            "https://www.googleapis.com/auth/plus.login",
            "https://www.googleapis.com/auth/userinfo.email",
            "https://www.googleapis.com/auth/userinfo.profile",
            "https://www.googleapis.com/auth/plus.me",
        ));
  
        return $client;
    }
}